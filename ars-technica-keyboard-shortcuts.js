// ==UserScript==
// @name         Ars Technica keyboard shortcut improvements
// @namespace    https://gitlab.com/thor84no/userscripts
// @version      0.1
// @description  Ars Technica implement some basic keyboard shortcuts, this extends those by adding shortcuts to improve the experience, especially when going through the comments section. Press '?' to see the full list.
// @author       Thor Saevareid
// @match        https://arstechnica.com/*
// @grant        GM_setValue
// @grant        GM_getValue
// ==/UserScript==

const selectors = {
    vote: {
        selected: 'button.vote-btn.selected',
        up: 'button.vote-btn-up',
        down: 'button.vote-btn-down',
        panel: 'aside.vote',
        sentiments: 'button.sentiment-btn'
    },
    comments: {
        selected: '#comments li.selected',
        toggleDisplay: '.toggle_hidden .action',
        toggleQuotes: '.quotetitle.unsquish',
        loadMore: '#more-comments a'
    },
    navigation: {
        previousPage: '#comments-area .subheading li:has(+ .selected) a',
        nextPage: '#comments-area .subheading.next-page a',
        lastPage: '.comment-pagination:first .subheading li:last',
        selectedPage: '.comment-pagination:first .subheading li.selected',
        links: '.body > a'
    }
};

(function() {
    'use strict';

    const css = {
        header: {
            'font-size': '1.2em',
            'font-weight': 'bold',
            'text-decoration': 'underline',
            margin: '1em 1.5em'
        },
        dialogue: {
            'background-color': 'black',
            'box-shadow': '0 0 8px hsla(0,0%,100%,.5)',
            'min-width': '600px',
            'min-height': '400px',
            'flex-direction': 'column'
        },
        entry: {
            display: 'flex',
            'flex-direction': 'row',
            'flex-wrap': 'nowrap'
        },
        container: {
            height: '100%',
            width: '100%',
            top: 0,
            left: 0,
            position: 'fixed',
            display: 'flex',
            'align-items': 'center',
            'justify-content': 'center'
        }
    };

    let hideCommentRatingUntilVoted = GM_getValue('hideCommentRatingUntilVoted');
    if (hideCommentRatingUntilVoted !== false) {
        hideCommentRatingUntilVoted = true;
    }

    const wrapCommentRatings = (vote) => {
        const nodes = vote.contents().toArray();
        const start = nodes.findIndex(node => $(node).is(selectors.vote.up)) + 1;
        const end = nodes.findIndex(node => $(node).is(selectors.vote.down));
        const range = nodes.slice(start, end);
        return $(range).wrap('<span class="commentRatingsWrapper"></span>').parent();
    };

    const showCommentRating = (show) => {
        return (vote) => {
            let ratings = vote.find('.commentRatingsWrapper');
            if (ratings.length === 0) {
                ratings = wrapCommentRatings(vote);
            }
            if (show) {
                ratings.show();
            } else {
                ratings.hide();
            }
        };
    };

    const refreshCommentRatingDisplayed = () => {
        if (hideCommentRatingUntilVoted) {
            const votes = $(selectors.vote.panel).toArray().map($);
            const commentsWithVote = votes.filter(vote => $(vote).find(selectors.vote.selected).length > 0);
            const commentsWithoutVote = votes.filter(vote => $(vote).find(selectors.vote.selected).length === 0);
            commentsWithVote.forEach(showCommentRating(true));
            commentsWithoutVote.forEach(showCommentRating(false));
        } else {
            const allComments = $(selectors.vote.panel).toArray().map($);
            allComments.forEach(showCommentRating(true));
        }
    };

    refreshCommentRatingDisplayed();

    $('.site-header').css({position: 'inherit'});

    ars.keyboard.press('r', 'Toggle hiding rating until voted', () => {
        hideCommentRatingUntilVoted = !hideCommentRatingUntilVoted;
        GM_setValue('hideCommentRatingUntilVoted', hideCommentRatingUntilVoted);
        refreshCommentRatingDisplayed();
    });
    ars.keyboard.press('a', 'Upvote comment', () => {
        if (ars.COMMENTS) {
            const selected = $(selectors.comments.selected);
            selected.find(selectors.vote.up).click();
            refreshCommentRatingDisplayed();
        }
    });
    ars.keyboard.press('z', 'Downvote comment', () => {
        if (ars.COMMENTS) {
            const selected = $(selectors.comments.selected);
            selected.find(selectors.vote.down).click();
            refreshCommentRatingDisplayed();
        }
    });
    ars.keyboard.press('x', 'Toggle display post', () => {
        if (ars.COMMENTS) {
            const selected = $(selectors.comments.selected);
            selected.find(selectors.comments.toggleDisplay).click();
        }
    });
    ars.keyboard.press('[', 'Previous page', () => {
        if (ars.COMMENTS) {
            $($(selectors.navigation.previousPage)[0]).click();
        }
    });
    ars.keyboard.press(']', 'Next page', () => {
        if (ars.COMMENTS) {
            $(selectors.navigation.nextPage).click();
            refreshCommentRatingDisplayed();
        }
    });
    ars.keyboard.press('m', 'Load more comments', () => {
        if (ars.COMMENTS) {
            $(selectors.comments.loadMore).click();
            refreshCommentRatingDisplayed();
        }
    });
    ars.keyboard.press('b', 'Open links in comment', () => {
        if (ars.COMMENTS) {
            const selected = $(selectors.comments.selected);
            selected.find(selectors.comments.links).toArray().forEach((link) => window.open(link.href));
        }
    });
    ars.keyboard.press('c', 'Toggle comments area', () => {
        const searchUrl = new URLSearchParams(window.location.search);
        if (searchUrl.get('comments')) {
            searchUrl.delete('comments');
        } else {
            searchUrl.set('comments', 1);
        }
        window.location.search = searchUrl.toString();
    });
    [1, 2, 3, 4, 5].forEach(n => {
        ars.keyboard.press(`${n}`, `Toggle sentiment ${n} (left-to-right)`, () => {
            if (ars.COMMENTS) {
                const selected = $(selectors.comments.selected);
                const buttons = selected.find(selectors.vote.sentiments).toArray().filter(sentiment => $(sentiment).is(':visible'));
                if (buttons.length >= n) {
                    buttons[n - 1].click();
                }
            }
        });
    });
    ars.keyboard.press('q', 'Toggle nested quotes', () => {
        if (ars.COMMENTS) {
            const selected = $(selectors.comments.selected);
            const unsquished = selected.find(selectors.comments.toggleQuotes).filter((_, element) => !$(element).parent().hasClass('__original__'));
            if (unsquished.length > 0) {
                const original = unsquished.parent().clone(true, true);
                original.addClass('__original__');
                original.hide();
                unsquished.click();
                unsquished.parent().parent().append(original);
            } else {
                const original = selected.find('.__original__');
                original.siblings('.quotecontent:not(.__original__)').replaceWith(original);
                original.removeClass('__original__');
                original.show();
            }
        }
    });

    ars.keyboard.press('`', 'List comment pages', () => {
        const openDialogue = $('#commentPageDialogue');
        if (openDialogue.length > 0) {
            return openDialogue.remove();
        }

        const header = $('<p/>', {
            text: 'Comments pages',
            css: css.header
        });

        const dialogue = $('<div/>', {
            css: Object.assign({}, css.dialogue, {
                'min-width': 'auto',
                'min-height': 'auto'
            })
        });
        dialogue.append(header);

        const pages = $('<p/>', {
            css: css.entry
        });
        dialogue.append(pages);

        const numberOfPages = parseInt($(selectors.navigation.lastPage).text(), 10);
        const selectedPage = parseInt($(selectors.navigation.selectedPage).text(), 10);
        for (let i = 1; i <= numberOfPages; i++) {
            const css = {
                'flex-grow': 1,
                'text-align': 'center',
                'min-width': '5em'
            };
            if (i === selectedPage) {
                Object.assign(css, {
                    'font-weight': 'bold',
                    color: '#ff4e00'
                });
            }
            pages.append($('<span/>', {
                text: i,
                css
            }));
        }
        const container = $('<div/>', {
            id: 'commentPageDialogue',
            css: css.container
        });

        container.append(dialogue);
        container.appendTo($('article'));
    });
    ars.keyboard.press('?', 'Toggle keyboard shortcuts help', () => {
        const openDialogue = $('#keyboardShortcutsHelp');
        if (openDialogue.length > 0) {
            return openDialogue.remove();
        }

        const handlers = ars.keyboard.handlers.map(function(handler) {
            return {
                type: handler[0],
                keyCode: handler[1],
                description: handler[3],
                key: handler[4]
            };
        });
        const keyPressHandlers = handlers.filter(function(handler) {
            return handler.type === 'press';
        });

        const header = $('<p/>', {
            text: 'Keyboard shortcuts',
            css: css.header
        });

        const dialogue = $('<div/>', {
            css: css.dialogue
        });
        dialogue.append(header);

        const keyboardShortcutsElements = keyPressHandlers
            .reduce(function(parent, handler) {
                const entry = $('<p/>', {
                    css: css.entry
                });
                entry.append($('<span/>', {
                    text: handler.key,
                    css: {
                        width: '30%',
                        'text-align': 'center'
                    }
                }));
                entry.append($('<span/>', {
                    text: handler.description
                }));
                parent.append(entry);
                return parent;
            }, dialogue);

        const container = $('<div/>', {
            id: 'keyboardShortcutsHelp',
            css: css.container
        });

        container.append(keyboardShortcutsElements);
        container.appendTo($('article'));
    });
})();

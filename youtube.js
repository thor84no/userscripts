// ==UserScript==
// @name         Disable Youtube autoplay
// @namespace    https://gitlab.com/thor84no/userscripts
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://www.youtube.com/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    const getToggle = () => {
        return new Promise((resolve) => {
            const toggle = document.getElementById('improved-toggle');
            if (toggle) {
                resolve(toggle);
            } else {
                setTimeout(() => getToggle().then(resolve), 100);
            }
        });
    }
    getToggle().then((toggle) => {
        if (toggle.active) {
            toggle.click();
        }
    });
})();
